import pdq
import sys

service_times = [0.003, 0.001, 0.01, 0.04, 0.1, 0.13, 0.15]
lambdas = [1.0, 194/143, 194/715, 291/715, 97/143, 102/143, 146/143]
assert len(service_times) == len(lambdas)

L = float(sys.argv[1])
D = list(map(lambda t: t[0]*t[1], zip(lambdas, service_times)))

WORK = "Traffik"
pdq.Init("DZ3")
pdq.CreateOpen(WORK, L)
pdq.SetWUnit("Packets")
pdq.SetTUnit("Seconds")

for i in range(len(D)):
    component_name = f"Component{i+1}"
    n = pdq.CreateNode(component_name, pdq.CEN, pdq.FCFS)
    pdq.SetDemand(component_name, WORK, D[i])

pdq.Solve(pdq.CANON)
pdq.Report()
