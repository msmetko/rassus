# DZ3 by Marijan Smetko

## Running instuctions

```bash
python3 -m venv venv
source venv/bin/activate
wget 'https://versaweb.dl.sourceforge.net/project/pdq-qnm-pkg/PDQ-Python%20Source/6.2.0/pdq-6.2.0.tar.gz'
tar xvf pdq-6.2.0.tar.gz
pushd pdq-6.2.0/
python3 setup.py build install
popd
python3 main.py 1 #replace '1' with your lambda
```
