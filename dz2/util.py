import socket
import time
import random
import math


class DatagramSocket(socket.socket):
    def __init__(self):
        super().__init__(socket.AF_INET, socket.SOCK_DGRAM)
        return

class DelayDatagramSocket(DatagramSocket):
    def __init__(self, delay, loss_rate):
        super().__init__()
        self.delay_duration = delay
        self.loss_rate = loss_rate
        return

    def delay(self):
        if random.random() > self.loss_rate:
            delay = random.uniform(0, 2*self.delay_duration)
            time.sleep(delay)
            return True
        return False

    def send(self, *args, **kwargs):
        return super().send(*args, **kwargs) if self.delay() else 0

    def sendto(self, *args, **kwargs):
        return super().sendto(*args, **kwargs) if self.delay() else 0


class RassusClock():
    def __init__(self):
        self.start_time = time.time()
        self.jitter = (random.randint(0, 400) - 200) / 1000
        return

    def time(self):
        current = time.time()
        diff = current - self.start_time
        coef = diff / 10
        return round(diff * math.pow((1+self.jitter), coef))