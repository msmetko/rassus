from collections import deque, namedtuple
import csv
import socket
import socketserver
from util import DelayDatagramSocket, RassusClock
import tempfile
import os
import time
import sys
import pathlib
import threading
import signal
from enum import Enum
import logging

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler(sys.stdout))
log.setLevel(os.environ.get('RASSUS_LOGLEVEL', logging.INFO))

class VectorClock():
    def __init__(self, scalars):
        self.scalars = scalars
        return

    def __lt__(self, other):
        if len(self.scalars) < len(other.scalars):
            return True
        n = len(self.scalars)
        return (sum(s <= t for s, t in zip(self.scalars, other.scalars)) == n
            and sum(s < t for s, t in zip(self.scalars, other.scalars)) >= 1)


def stringify(x):
    if isinstance(x, tuple) and len(x) == 3:
        return "{}|{}|{}".format(x[0], ",".join(map(str, x[1])), x[2])
    else:
        return str(x)

class MessageType(Enum):
    MEASUREMENT = "MEASUREMENT"
    CONFIRM = "CONFIRM"
    UDPNODE = "UDPNODE"
    BYEBYE = "BYEBYE"


Request = namedtuple('Request', ('type', 'content', 'address', 'port'))

CONFIG_DIR = pathlib.Path('config/')
ADDRESS = '127.0.0.1'

class UDPNode():
    def __init__(self):
        CONFIG_DIR.mkdir(exist_ok=True)
        with open('mjerenja.csv', 'r') as file:
            self.readings = list(csv.DictReader(file))
        self.server_socket = DelayDatagramSocket(0.4, 0.1)
        self.other_nodes = []
        self.requests = deque()
        self.confirmed = deque()
        self.recieved = deque()
        self.measurements = deque()
        self.request_lock = threading.Lock()
        self.clock_lock = threading.Lock()
        for file in CONFIG_DIR.iterdir():
            ip, port = file.read_text().strip().split(',')
            self.other_nodes.append((ip, int(port)))
        self.NODE_ID = len(self.other_nodes)
        self.global_clock = 0
        self.clock = RassusClock()
        log.debug(self.clock.jitter)
        self.vector_clocks = [0 for _ in range(self.NODE_ID+1)]
        log.debug(f"ID = {self.NODE_ID}")
        self.stop_threads = False
        signal.signal(signal.SIGINT, self.shutdown)
        #log.debug(f"{self.readings[0], self.readings[1], self.readings[-1])
        return

    def __enter__(self):
        self.server_socket.bind((ADDRESS, 0))
        self.config = tempfile.NamedTemporaryFile(dir=CONFIG_DIR)
        self.config.write("{},{}".format(*self.server_socket.getsockname()).encode("UTF-8"))
        self.config.flush()
        message = "UDPNODE\n{},{}".format(*self.server_socket.getsockname()).encode("UTF-8")
        send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        for node in self.other_nodes:
            send_socket.sendto(message, node)
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.server_socket.close()
        self.config.close()
        if not next(CONFIG_DIR.iterdir(), False):
            CONFIG_DIR.rmdir()
        return

    def add_request(self, request):
        self.request_lock.acquire()
        if request not in self.requests:
            self.requests.append(request)
        self.request_lock.release()
        return

    def send_all(self):
        def f(message, address, port):
            self.server_socket.sendto(message, (address, port))
            return
        self.request_lock.acquire()
        for _ in range(len(self.requests)):
            request = self.requests.popleft()
            message_type, message_content, address, port = request
            message = f"{message_type.value}\n{stringify(message_content)}"
            thread = threading.Thread(target=f, args=(message.encode("UTF-8"), address, port))
            thread.start()
            if message_type != MessageType.CONFIRM:
                self.requests.append(request)
        self.request_lock.release()
        return

    def measurement_loop(self):
        i = 0
        while True:
            for _ in range(5):
                log.debug("SEND")
                i += 1
                if self.stop_threads:
                    return
                for node in self.other_nodes:   
                    # node is tuple(node_address, node_port)
                    measure_index = i % len(self.readings)
                    measurement = int(self.readings[measure_index]["CO"])
                    log.debug(f"Sending {measurement}")
                    node_address, node_port = node
                    self.clock_lock.acquire()
                    self.global_clock += 1
                    self.vector_clocks[self.NODE_ID] += 1
                    request = Request(MessageType.MEASUREMENT,
                                (self.global_clock, self.vector_clocks, measurement),
                                node_address, node_port)
                    self.clock_lock.release()
                    self.add_request(request)

                self.send_all()
                time.sleep(1.0)
            if len(self.measurements) > 0:
                s = 0
                for measurement in sorted(self.measurements, key=lambda t: t[0]):
                    log.info(f"\t {measurement[0]} {measurement[-1]}")
                    s += measurement[-1]
                s /= len(self.measurements)
                log.info("")
                for measurement in sorted(self.measurements, key=lambda t: VectorClock(t[1])):
                    log.info(f"\t {measurement[1]} {measurement[-1]}")
                log.info(f"Average: {s}")
                self.measurements.clear()
        return

    def serve_forever(self):
        self.server_socket.settimeout(0.1)
        while True:
            try:
                (message, (sender_address, sender_port)) = self.server_socket.recvfrom(1024)
                message = message.decode("UTF-8")
                message_type, message = message.split('\n')
                message_type = MessageType(message_type)
                if message_type == MessageType.UDPNODE:
                    address, port = message.split(',')
                    self.other_nodes.append((address, int(port)))
                    self.clock_lock.acquire()
                    self.vector_clocks.append(0)
                    self.global_clock += 1
                    self.clock_lock.release()
                elif message_type == MessageType.BYEBYE:
                    address, port, node_id = message.split(',')
                    self.other_nodes.remove((address, int(port)))
                    self.request_lock.acquire()
                    to_remove = [t for t in self.requests if t.port == int(port)]
                    for t in to_remove:
                        self.requests.remove(t)
                    self.request_lock.release()
                    self.clock_lock.acquire()
                    self.vector_clocks.pop(int(node_id))
                    if int(node_id) < self.NODE_ID:
                        self.NODE_ID -= 1
                    self.vector_clocks[self.NODE_ID] += 1
                    self.global_clock += 1
                    self.clock_lock.release()
                elif message_type == MessageType.MEASUREMENT:
                    clock_str, vclock_str, value = message.split('|')
                    clock = int(clock_str)
                    vclock = list(map(int, vclock_str.split(',')))
                    request = Request(MessageType.CONFIRM, clock, sender_address, sender_port)
                    self.add_request(request)
                    if (sender_port, clock) not in self.recieved:
                        self.recieved.append((sender_port, clock))
                        self.measurements.append((clock, vclock, float(value)))
                    self.clock_lock.acquire()
                    self.global_clock = max(self.clock.time(), clock) + 1
                    self.vector_clocks[self.NODE_ID] += 1
                    for i in range(min(len(self.vector_clocks), len(vclock))):
                        if i != self.NODE_ID:
                            self.vector_clocks[i] = max(self.vector_clocks[i], vclock[i]) + 1
                    self.clock_lock.release()
                elif message_type == MessageType.CONFIRM:
                    log.debug(f"Confirmed: {message} {sender_port}")
                    clock = int(message)
                    self.request_lock.acquire()
                    to_remove = list(t for t in self.requests if t.type == MessageType.MEASUREMENT and t.content[0] == clock and t.port == sender_port)
                    for t in to_remove:
                        self.requests.remove(t)
                    self.request_lock.release()
                    log.debug(f"LEN: {len(list(t for t in self.requests if t.type == MessageType.MEASUREMENT and t.port == sender_port))}")
                if self.stop_threads:
                    return
            except socket.timeout:
                if self.stop_threads:
                    break
        return

    def start(self):
        self.client_thread = threading.Thread(target=self.measurement_loop)
        self.client_thread.start()
        self.serve_forever()
        self.client_thread.join()
        return
    
    def shutdown(self, signal, frame):
        print('gracefully shutting down')
        self.stop_threads = True
        bye_bye_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        message = "BYEBYE\n{},{},{}".format(*self.server_socket.getsockname(), self.NODE_ID).encode("UTF-8")
        for node in self.other_nodes:
            bye_bye_socket.sendto(message, node)
        return


if __name__ == "__main__":
    with UDPNode() as node:
        node.start()