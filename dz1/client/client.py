import csv
import random
import requests
import string
import socket
import sys
import threading
import time

NAME_LENGTH = 8
LONGITUDE_RANGE = (15.87, 16.0)
LATITUDE_RANGE = (45.75, 45.85)

BUF_SIZE = 4096


def get_random_name():
    return ''.join(random.choices(string.ascii_letters+string.digits, k=NAME_LENGTH))


def measurement_to_string(measurement: dict):
    return "\n".join("{} {}".format(k, 0 if v == '' else v) for k, v in measurement.items())


def string_to_measurement(string: str):
    return dict(map(
        lambda kv: (kv[0], None if kv[1] == '' else int(kv[1])),
        map(str.split, string.split('\n'))))


class TCPWorker(threading.Thread):
    def __init__(self, socket, address, client):
        super().__init__()
        self.socket = socket
        self.address = address
        self.client = client
        return

    def run(self):
        message_bytes = self.socket.recv(4096)
        message = message_bytes.decode()
        print(f"Sensor {message} wants my data")
        while True:
            elapsed_seconds = self.client.seconds_elapsed()
            measurement = self.client.get_measurement(elapsed_seconds)
            response = measurement_to_string(measurement)
            try:
                self.socket.send(response.encode("UTF-8"))
                time.sleep(1.0)
            except BrokenPipeError:
                print(f"Sensor {message} does not want data anymore. Aborting.")
                break
        return


class TCPServer(threading.Thread):
    def __init__(self, socket, client):
        super().__init__()
        self.socket = socket
        self.client = client
        return

    def run(self):
        print("Server started")
        self.socket.listen()
        while True:
            client_socket, client_address = self.socket.accept()
            print("A sensor connected")
            worker = TCPWorker(client_socket, client_address, self.client)
            worker.start()
        return


class Client():
    def __init__(self, server_ip, server_port, measurements_filename, mode=0):
        self.server_ip = server_ip
        self.server_port = server_port
        self.mode = mode
        self.name = get_random_name()
        self.latitude = random.uniform(*LATITUDE_RANGE)
        self.longitude = random.uniform(*LONGITUDE_RANGE)
        with open(measurements_filename, 'r') as file:
            reader = csv.DictReader(file, delimiter=',', restval=0)
            self.measurements = list(reader)
        self.socket = socket.socket()
        self.socket.bind(("127.0.0.1", 0))
        self.tcp = TCPServer(self.socket, self)
        self.start_time = time.time()
        return

    def register(self):
        response = requests.post(f'http://{self.server_ip}:{self.server_port}/sensor', json={
            "username": self.name,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "port": self.socket.getsockname()[1]})
        return response.status_code

    def start(self):
        self.tcp.start()
        if self.mode != 0:
            input("Press enter to start sending sending measurements")
        try:
            while True:
                elapsed_seconds = self.seconds_elapsed()
                measurement = self.get_measurement(elapsed_seconds)
                measurement = string_to_measurement(measurement_to_string(measurement))
                response = requests.get(f"http://{self.server_ip}:{self.server_port}/sensor", json={
                    "username": self.name
                })
                sensor_socket = socket.socket()
                closest_sensor_data = response.json()
                sensor_socket.connect((closest_sensor_data["ip"], closest_sensor_data["port"]))
                sensor_socket.send(self.name.encode("UTF-8"))
                msg_bytes = sensor_socket.recv(BUF_SIZE)
                sensor_socket.shutdown(socket.SHUT_RDWR)
                sensor_socket.close()
                closest_measurement = string_to_measurement(msg_bytes.decode("UTF-8"))
                average = dict()
                for (k1, v1), (k2, v2) in zip(measurement.items(), closest_measurement.items()):
                    assert k1 == k2, f"{k1} != {k2}"
                    if v1 == 0:
                        average[k2] = v2
                    elif v2 == 0:
                        average[k1] = v1
                    else:
                        average[k1] = (v1 + v2) / 2
                print(f"Average: {average}")
                response = requests.post(
                    f"http://{self.server_ip}:{self.server_port}/measurement",
                    json=average)

                random_time = random.uniform(0.5, 1.5)
                time.sleep(random_time)
        except requests.exceptions.ConnectionError:
            print(f"Server @ {self.server_ip}:{self.server_port} unavailable. Aborting.")
            sys.exit(0)
        return

    def seconds_elapsed(self):
        return int(time.time() - self.start_time)

    def get_measurement(self, elapsed_seconds):
        measurement_index = elapsed_seconds % len(self.measurements) + 2
        print(f"Elapsed time: {elapsed_seconds}\tIndex: {measurement_index}")
        measurement = self.measurements[measurement_index]
        return measurement


if __name__ == '__main__':
    server_ip = sys.argv[1] if len(sys.argv) >= 2 else "127.0.0.1"
    server_port = int(sys.argv[2]) if len(sys.argv) >= 3 else 1204
    mode = int(sys.argv[3]) if len(sys.argv) >= 4 else 1
    client = Client(server_ip, server_port, 'mjerenja.csv', mode)
    status = client.register()
    if status != 201:
        print(f"Client {client.name} failed to register. Aborting.")
        sys.exit(1)
    client.start()
