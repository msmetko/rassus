# DZ1

## 0. Environment
This homework is written in Python3 with 2 extra libraries. Please setup the environment with:
```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
## 1. Running the server
Just call `python3 server.py` from the `server/` directory.
Note: virtual environment should be activated.

## 2. Running the clients
To run clients, it is recommended that the first client is run in manual mode, and others afterwards can be ran in automatic mode.
To run a client in a manual mode, run this:
`python3 client.py '127.0.0.1' 1204 1`. This will connect the server at `127.0.0.1:1204`.
To run a client in auto mode, run `python3 client.py '127.0.0.1' 1204` or omit the parameters altogether, such as `python3 client.py`.

If the client is run in automode, it'll register with the server and it'll start measuring and communicating on its own. A manual client register with the server but waits for the user to press enter to start measuring and communicating.

Note, all of these client runnning commands should be ran from the `client/` directory. Running them from any other directory will make the client fail. Also, virtual environment should, just as above, be also activated.
