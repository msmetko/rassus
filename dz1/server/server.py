from dataclasses import dataclass
from flask import Flask, request
from logging import INFO as LOG_INFO
import math

R = 6317
SENSORS = []
app = Flask(__name__)
app.logger.setLevel(LOG_INFO)


@dataclass
class Sensor():
    username: str
    latitude: float
    longitude: float
    ip: str
    port: int

    def distance_to(self, sensor: 'Sensor'):
        dlon = math.radians(sensor.longitude - self.longitude)
        dlat = math.radians(sensor.latitude - self.latitude)
        a = math.sin(dlat/2) ** 2
        a += math.cos(self.latitude)*math.cos(sensor.latitude)*(math.sin(dlon/2) ** 2)
        c = 2*math.atan2(math.sqrt(a), math.sqrt(1-a))
        distance = c * R
        app.logger.info(f"\t{self.username}<->{sensor.username}: {distance}")
        return distance


@app.route('/sensor', methods=['POST'])
def register():
    request_data = request.json
    sensor_name = request_data['username']
    if sensor_name in map(lambda s: s.username, SENSORS):
        app.logger.error(f"\tA sensor tried to register with name {sensor_name} which was taken")
        return "The sensor with this name is alredy registered", 409  # Conflict
    sensor_latitude = request_data['latitude']
    sensor_longitude = request_data['longitude']
    sensor_ip = request.remote_addr
    sensor_port = request_data['port']
    sensor = Sensor(sensor_name, sensor_latitude, sensor_longitude, sensor_ip, sensor_port)

    SENSORS.append(sensor)
    app.logger.info(f"\tRegistered sensor {sensor.username} ({sensor.latitude}, {sensor.longitude})"
                    + f" from {sensor.ip}:{sensor.port}")
    return '', 201


@app.route('/sensor', methods=["GET"])
def closest():
    request_data = request.json
    sensor_name = request_data['username']
    sensors_list = list(filter(lambda s: s.username == sensor_name, SENSORS))
    assert len(sensors_list) == 1, f"len(sensors_list) == {len(sensors_list)}"
    sensor = sensors_list[0]
    sensors_sorted = sorted(
        filter(lambda s: s != sensor, SENSORS),
        key=lambda s: sensor.distance_to(s))
    closest = sensors_sorted[0]
    return {"ip": closest.ip, "port": closest.port}, 200, {"Content-Type": "application/json"}


@app.route('/measurement', methods=["POST"])
def store_measurement():
    request_data = request.json
    app.logger.info(f"\tRecieved a measurement: {repr(request_data)}")
    return '', 200


if __name__ == '__main__':
    app.run('127.0.0.1', 1204, debug=True)
